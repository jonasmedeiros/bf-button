import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'bf-button',
  template: `
    <button [class]="buttonStyle">
      <ng-content></ng-content>
    </button>
  `,
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() type: string = 'primary';
  @Input() size: string = 'medium';
  @Input() full: boolean = false;

  private buttonStyle: string;

  constructor() { }

  ngOnInit() {

    this.buttonStyle = `${this.type} `;
    this.buttonStyle += `${this.size} `;

    if (this.full) {
      this.buttonStyle += 'full-width';      
    }
    
  }

}

import { BorrowersfirstButtonPage } from './app.po';

describe('borrowersfirst-button App', () => {
  let page: BorrowersfirstButtonPage;

  beforeEach(() => {
    page = new BorrowersfirstButtonPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
